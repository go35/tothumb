module zrpc

go 1.15

require (
	github.com/Arafatk/glot v0.0.0-20180312013246-79d5219000f0 // indirect
	github.com/BurntSushi/graphics-go v0.0.0-20160129215708-b43f31a4a966
	github.com/ajstarks/svgo v0.0.0-20200725142600-7a3c8b57fecb // indirect
	github.com/aliyun/aliyun-oss-go-sdk v2.1.6+incompatible
	github.com/bitly/go-simplejson v0.5.0
	github.com/corona10/goimagehash v1.0.3
	github.com/gin-gonic/gin v1.6.3
	github.com/go-latex/latex v0.0.0-20210118124228-b3d85cf34e07 // indirect
	github.com/go-redis/redis v6.15.9+incompatible // indirect
	github.com/go-sql-driver/mysql v1.5.0
	github.com/golang/freetype v0.0.0-20170609003504-e2365dfdc4a0
	github.com/gomodule/redigo v1.8.4
	github.com/jinzhu/gorm v1.9.16
	github.com/joshdk/preview v0.0.0-20171101184055-9bc34624576d
	github.com/joshdk/quantize v0.0.0-20171110221748-65999d3a4c76
	github.com/natefinch/lumberjack v2.0.0+incompatible
	github.com/rwcarlsen/goexif v0.0.0-20190401172101-9e8deecbddbd
	github.com/svent/go-flags v0.0.0-20141123140740-4bcbad344f03 // indirect
	github.com/svent/go-nbreader v0.0.0-20150201200112-7cef48da76dc // indirect
	github.com/svent/sift v0.9.0 // indirect
	github.com/unknwon/goconfig v0.0.0-20200908083735-df7de6a44db8
	go.uber.org/zap v1.16.0
	golang.org/x/image v0.0.0-20210220032944-ac19c3e999fb
	golang.org/x/text v0.3.5 // indirect
	golang.org/x/time v0.0.0-20210220033141-f8bda1e9f3ba // indirect
	gonum.org/v1/plot v0.8.1 // indirect
)
