package main

import (
	"encoding/json"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/unknwon/goconfig"
	"io"
	"os"
	"runtime"
	"strconv"
	"sync"
	"time"
	"zrpc/common"
	"zrpc/log"
	"zrpc/util"
)

var wg sync.WaitGroup
func init()  {
	// 软件目录获取
	common.SoftDir = "."
	if dir, err := os.Getwd(); err == nil {
		common.SoftDir = dir
	}

	// 日志初始化
	if !common.IsDir(common.SoftDir + "/logs/") {
		_ = os.Mkdir(common.SoftDir+"/logs/", 0777)
	}

	// 临时目录初始化
	if !common.IsDir(common.SoftDir + "/temp/") {
		_ = os.Mkdir(common.SoftDir+"/temp/", 0777)
	}

	// 读取conf配置
	var err error
	if common.Config, err = goconfig.LoadConfigFile(common.SoftDir + "/conf.ini"); err != nil {
		log.Println("配置文件加载失败，程序退出")
		os.Exit(0)
	}

	// 禁用控制台颜色，将日志写入文件时不需要控制台颜色。
	gin.DisableConsoleColor()

	// gin框架日志配置
	var fp *os.File
	fileName := fmt.Sprintf("%s/logs/gin_%s.log", common.SoftDir, time.Now().Format("20060102"))
	_, err = os.Stat(fileName)
	if err == nil {
		fp, _ = os.OpenFile(fileName, os.O_APPEND, 6)
	} else {
		fp, _ = os.Create(fileName)
	}
	gin.DefaultWriter = io.MultiWriter(fp)

	// redis初始化
	common.ClientRedis = common.NewRedisPool()
}

func main() {
	// 设置并发线程数，可以使用 runtime.NumCPU() 查看CPU核心数
	runtime.GOMAXPROCS(runtime.NumCPU())

	wg.Add(2)
	// 监听redis队列
	go func() {
		defer wg.Done()
		util.Timer(util.TimerFunc)
	}()
	go func() {
		defer wg.Done()
		util.PackTimer(util.PackTimerFunc)
	}()
	// 启动gin
	//go func() {
	//	defer wg.Done()
	//	ginRun()
	//}()
	wg.Wait()
}

// 启动gin
func ginRun()  {
	r := gin.Default()
	r.POST("/thumb/compress", func(c *gin.Context) {
		var form util.JsonData
		if c.ShouldBind(&form) == nil {
			//初始化缩略对象
			thumb := util.NewThumb()
			thumb.InitThumb(form)
			thumb.AutoCompress()
			if thumb.CompressStatus == true {
				data := map[string] string {
					"md5" : thumb.ThumbModel.Md5,
					"origin_url" : thumb.ThumbModel.Origin_url,
					"thumb_url" : thumb.ThumbModel.Thumb_url,
					"mark_url" : thumb.ThumbModel.Mark_url,
					"tags" : thumb.ThumbModel.Tags,
					"width" : strconv.Itoa(int(thumb.ThumbModel.Width)),
					"height" : strconv.Itoa(int(thumb.ThumbModel.Height)),
					"scale" : strconv.FormatFloat(thumb.ThumbModel.Scale, 'f', -1, 64),
					"size" : strconv.Itoa(int(thumb.ThumbModel.Size)),
					"long" : thumb.ThumbModel.Long,
					"lat" : thumb.ThumbModel.Lat,
				}
				c.JSON(200, gin.H{
					"message": "处理成功",
					"data": data,
				})
			} else {
				c.JSON(403, gin.H{"message": "处理失败"})
			}
		} else {
			c.JSON(400, gin.H{"message": "参数不正确"})
		}
	})
	r.POST("/thumb/push", func(c *gin.Context) {
		var form util.JsonData
		if c.ShouldBind(&form) == nil {
			// 获取任务数据
			cr := common.ClientRedis.Get()
			defer func() {
				cr.Close()
			}()
			if cr.Err() != nil {
				c.JSON(400, gin.H{"message": "ClientRedis.Get错误"})
				return
			}
			data, _ := json.Marshal(form)
			_, err := cr.Do("lpush", "goThumbQueue", data)
			if err != nil {
				c.JSON(400, gin.H{"message": "redis.lpush错误"})
				return
			}
			c.JSON(200, gin.H{"message": "push成功"})
		} else {
			c.JSON(400, gin.H{"message": "参数不正确"})
		}
	})
	log.Println("启动gin服务，监听:8989端口")
	r.Run(":8989") // 监听并在 0.0.0.0:8080 上启动服务
}


