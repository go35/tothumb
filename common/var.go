package common

import (
	"github.com/gomodule/redigo/redis"
	"github.com/unknwon/goconfig"
)

var (
	SoftDir string
	ClientRedis *redis.Pool
	Config *goconfig.ConfigFile
)