package common

import (
	"crypto/md5"
	"encoding/base64"
	"encoding/hex"
	"encoding/json"
	"github.com/gomodule/redigo/redis"
	"io/ioutil"
	"net/http"
	url2 "net/url"
	"os"
	"sort"
	"strconv"
	"strings"
	"time"
	"zrpc/log"
)

func IsEnvByProduction() bool {
	env := Config.MustValue("config", "env")
	if env == "production" {
		return true
	} else {
		return false
	}
}

func IsDir(path string) bool {
	s, err := os.Stat(path)
	if err != nil {
		return false
	}
	return s.IsDir()
}

func In(str string, str_array []string) bool {
	sort.Strings(str_array)
	index := sort.SearchStrings(str_array, str)
	if index < len(str_array) && str_array[index] == str {
		return true
	}
	return false
}

func CallBackFun(url string, md5 string, thumbUrl string, markUrl string) {
	p := url2.Values{}
	p.Add("md5", md5)
	p.Add("thumbUrl", thumbUrl)
	p.Add("markUrl", markUrl)
	url += "?" + p.Encode()
	_, err := http.Get(url)
	if err != nil {
		log.Println("http get 回调 错误", err.Error())
	}
}

func Md5Str(text string) string {
	h := md5.New()
	h.Write([]byte(text))
	return hex.EncodeToString(h.Sum(nil))
}

func Base64url(text []byte) string {
	str := base64.StdEncoding.EncodeToString(text)
	str = strings.Replace(str, "+", "-", -1)
	str = strings.Replace(str, "/", "_", -1)
	str = strings.Replace(str, "=", "",-1)
	return str
}

func OssShowUrl(url string, endpoiont string, bucketName string) string {
	if strings.Index(url, ".aliyuncs.com") < 0 {
		//endpoiont := Config.MustValue("oss", "endpoiont")
		//bucketName := Config.MustValue("oss", "bucket_name")
		url = strings.Replace(endpoiont, "oss-cn-", bucketName + ".oss-cn-", 1) + "/" + url
	}
	return url
}

// 返回redis连接池
func NewRedisPool() *redis.Pool {
	return &redis.Pool{
		MaxIdle:     100, // 空闲最大连接数
		MaxActive:   1200, // 允许最大连接数
		IdleTimeout: 10 * time.Second, // 空闲连接超时时间，超过超时时间的空闲连接会被关闭。
		Wait:        true, //如果Wait被设置成true，则Get()方法将会阻塞
		Dial: func() (redis.Conn, error) {
			host := Config.MustValue("redis", "host") + ":" + Config.MustValue("redis", "port")
			password := Config.MustValue("redis", "password")
			db, _ := strconv.Atoi(Config.MustValue("redis", "db"))
			con, err := redis.Dial("tcp", host,
				redis.DialPassword(password),
				redis.DialDatabase(db),
				redis.DialConnectTimeout(10 * time.Second),
				redis.DialReadTimeout(30 * time.Second),
				redis.DialWriteTimeout(30 * time.Second))
			if err != nil {
				return nil, err
			}
			return con, nil
		},
	}
}

// 高德地图API-逆地理编码
func AmapGeocodeRegeo(long string, lat string) string {
	location := AmapAssistantConvert(long, lat)
	if location == "" {
		return ""
	}
	key := Config.MustValue("amap", "key")
	p := url2.Values{}
	p.Add("key", key)
	p.Add("location", location)
	url := "https://restapi.amap.com/v3/geocode/regeo?" + p.Encode()
	resp, err := http.Get(url)
	address := ""
	if err != nil {
		log.Println("http get 错误", err.Error())
		return address
	}
	defer resp.Body.Close()
	body, _ := ioutil.ReadAll(resp.Body)
	var jdata interface{}
	json.Unmarshal(body, &jdata)
	m := jdata.(map[string]interface{})
	if m["status"].(string) == "1" {
		regeo := m["regeocode"].(map[string]interface{})
		address = regeo["formatted_address"].(string)
	}
	return address
}

// 高德地图API-坐标转换API服务地址
func AmapAssistantConvert(long string, lat string) string {
	key := Config.MustValue("amap", "key")
	p := url2.Values{}
	p.Add("key", key)
	p.Add("locations", long + "," + lat)
	p.Add("coordsys", "gps")
	url := "https://restapi.amap.com/v3/assistant/coordinate/convert?" + p.Encode()
	resp, err := http.Get(url)
	location := ""
	if err != nil {
		log.Println("http get 错误", err.Error())
		return location
	}
	defer resp.Body.Close()
	body, _ := ioutil.ReadAll(resp.Body)
	var jdata interface{}
	json.Unmarshal(body, &jdata)
	m := jdata.(map[string]interface{})
	if m["status"].(string) == "1" {
		location = m["locations"].(string)
	}
	return location
}
