package util

import (
	simplejson "github.com/bitly/go-simplejson"
	"io/ioutil"
	"net/http"
	"strings"
	"sync"
	"zrpc/common"
	"zrpc/core"
)

type Huawei struct {
	signer core.Signer
}

var (
	instance *Huawei
	signer *core.Signer
	once sync.Once
)

func GetHuaweiInstance() *Huawei {
	once.Do(func() {
		instance = &Huawei{}
		instance.init()
	})
	return instance
}

func (this *Huawei) GetImageTagging(imageUrl string) string {
	// 生成一个新的Request，指定域名、方法名、请求url和body
	endpoint := "image.cn-north-4.myhuaweicloud.com"
	data := "{'image':'', 'url':'" + imageUrl + "', 'language':'zh', 'threshold':'5', 'limit':'10'}"
	r, _ := http.NewRequest("POST", "https://" + endpoint + "/v1.0/image/tagging", strings.NewReader(data))
	// 添加请求头
	r.Header.Add("content-type", "application/json")
	// 创建签名
	this.signer.Sign(r)
	// 访问API 获取请求结果
	resp, err := http.DefaultClient.Do(r)
	if err != nil {
		panic("请求API异常：" + err.Error())
	}
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		panic("resp.Body异常：" + err.Error())
	}
	// 遍历JSON数据
	tags := []string{}
	jdata, err := simplejson.NewJson(body)
	if jdata.Get("result") != nil {
		rows, _ := jdata.Get("result").Get("tags").Array()
		for _, row := range rows {
			if each_map, ok := row.(map[string]interface{}); ok {
				tags = append(tags, each_map["tag"].(string))
			}
		}
	}
	return strings.Join(tags, ",")
}

// 对象初始化
func (this *Huawei) init() {
	// 生成一个新的Signer，分别输入AK和SK值。
	this.signer = core.Signer{
		Key: common.Config.MustValue("huawei", "key"),
		Secret: common.Config.MustValue("huawei", "secret"),
	}
}
