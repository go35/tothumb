package util
/**
文件打包
 */

import (
	"archive/zip"
	"encoding/json"
	"github.com/aliyun/aliyun-oss-go-sdk/oss"
	"io"
	"net/http"
	"net/url"
	"os"
	"path/filepath"
	"strings"
	"time"
	"zrpc/common"
	"zrpc/log"
	url2 "net/url"
)

type PackJsonData struct {
	TaskId string
	FileName string
	CallBack string
	AccessKeyId string
	AccessKeySecret string
	Cdn string
	Files []PackJsonFile
}

type PackJsonFile struct {
	OssFile string
	SavePath string
}

// 定时器 2000毫秒一次心跳包
func PackTimer(timer func()) {
	log.Println("开启程序，监听goPackQueue队列")
	ticker := time.NewTicker(5 * time.Second)
	for {
		select {
		case <-ticker.C:
			timer()
		}
	}
}

func PackTimerFunc() {
	// 获取任务数据
	cr := common.ClientRedis.Get()
	defer func() {
		cr.Close()
	}()
	if cr.Err() != nil {
		log.Println("ClientRedis.Get错误", cr.Err().Error())
		return
	}
	lock, err := cr.Do("exists", "goPackLock")
	if err == nil && lock.(int64) == 1 {
		log.Println("目前pack任务正在处理中")
		return
	}
	data, err := cr.Do("rpop", "goPackQueue")
	if err == nil && data != nil {
		jdata := PackJsonData{}
		err := json.Unmarshal(data.([]byte), &jdata)
		if err == nil {
			cr.Do("set", "goPackLock", 1, "ex", 86400)
			log.Println("开始处理pack队列数据:", jdata.TaskId)
			//初始化缩略对象
			RunPack(jdata)
			cr.Do("del", "goPackLock")
		} else {
			log.Println("pack队列数据解析错误", data)
		}
	}
}

func RunPack(Data PackJsonData)  {
	u, _ := url.Parse(Data.Cdn)
	host := strings.Split(u.Host, ".")
	bucketName := host[0]
	endpoiont := strings.Replace(Data.Cdn, host[0] + ".", "", 1)
	if common.IsEnvByProduction() == true {
		//正式环境
		endpoiont = strings.Replace(endpoiont, ".aliyuncs.com", "-internal.aliyuncs.com", 1)
	}
	ossClient, err := oss.New(endpoiont, Data.AccessKeyId, Data.AccessKeySecret)
	if err != nil {
		log.Println("OSS初始化失败", err.Error())
		return
	}
	// 获取存储空间
	ossBucket, err := ossClient.Bucket(bucketName)
	if err != nil {
		log.Println("OSS初始化失败", err.Error())
		return
	}
	//遍历下载图片到本地
	basePath := common.SoftDir + "/temp/" + strings.TrimSuffix(filepath.Base(Data.FileName), filepath.Ext(Data.FileName)) + "/"
	os.MkdirAll(basePath, os.ModePerm)
	for idx, item := range Data.Files {
		// 下载文件到本地文件
		filePath := basePath + filepath.Base(item.OssFile)
		err = ossBucket.GetObjectToFile(item.OssFile, filePath)
		if err != nil {
			log.Println("OSS Error:", err)
		} else {
			Data.Files[idx].OssFile = filePath
		}
	}
	zipFile := ZipPack(basePath, Data)
	// 分片大小10Mb，5个协程并发上传分片，使用断点续传。
	// 其中"<yourObjectName>"为objectKey， "LocalFile"为filePath，10*1024*1024为partSize。
	objectKey := "gozip/"+Data.FileName
	err = ossBucket.UploadFile(objectKey, zipFile, 20*1024*1024, oss.Routines(5), oss.Checkpoint(true, ""))
	if err != nil {
		log.Println("ossBucket.UploadFile Error:", err)
		return
	}
	log.Printf("pack任务完成---TaskId: %s , objectKey: %s", Data.TaskId, objectKey)
	//删除临时文件
	PackRemoveAll(basePath)
	//回调通知
	if Data.CallBack != "" {
		PackCallBackFun(Data.CallBack, Data.TaskId, objectKey)
	}
}

//创建zip文件
func ZipPack(basePath string, Data PackJsonData) string {
	zipFileName := basePath + Data.FileName
	fw, err := os.Create(zipFileName)
	if err != nil {
		log.Println("zip文件创建失败:", err)
	}
	zw := zip.NewWriter(fw)
	defer func() {
		// 检测一下是否成功关闭
		if err := zw.Close(); err != nil {
			log.Println("zw.Close Error:", err)
		}
		fw.Close()
	}()
	for _, item := range Data.Files {
		fr, err := os.Open(item.OssFile)
		if err != nil {
			continue
		}
		fi, err := fr.Stat()
		if err != nil {
			continue
		}
		if string(item.SavePath[0]) == "/" {
			item.SavePath = item.SavePath[1:]
		}
		// 写入文件的头信息
		fh, err := zip.FileInfoHeader(fi)
		fh.Method = zip.Deflate
		fh.Name = item.SavePath
		w, err := zw.CreateHeader(fh)
		if err != nil {
			fr.Close()
			continue
		}
		// 写入文件内容
		_, err = io.Copy(w, fr)
		fr.Close()
		if err != nil {
			continue
		}
	}
	return zipFileName
}

//pack回调函数
func PackCallBackFun(url string, taskId string, ossFile string) {
	p := url2.Values{}
	p.Add("taskId", taskId)
	p.Add("ossFile", ossFile)
	url += "?" + p.Encode()
	_, err := http.Get(url)
	if err != nil {
		log.Println("http get 回调 错误", err.Error())
	}
}

//删除临时文件
func PackRemoveAll(path string) {
	filepath.Walk(path, func(path string, fi os.FileInfo, err error) error {
		if nil == fi {
			return err
		}
		err2 := os.RemoveAll(path)
		if err2 != nil {
			log.Println("PackRemoveAll Error:", err2)
		}
		return nil
	})
}
