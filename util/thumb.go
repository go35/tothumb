package util
/**
图片解码
 */
import (
	"bytes"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"github.com/aliyun/aliyun-oss-go-sdk/oss"
	"github.com/joshdk/quantize"
	"github.com/rwcarlsen/goexif/exif"
	_ "golang.org/x/image/bmp"
	"image"
	"image/color"
	_ "image/gif"
	"image/jpeg"
	_ "image/png"
	"math"
	url2 "net/url"
	"os"
	"os/exec"
	"path"
	"strconv"
	"strings"
	"time"
	"zrpc/common"
	"zrpc/log"
	"zrpc/model"
)

type JsonData struct {
	Id string `form:"id" binding:"required"`
	Md5 string `form:"md5" binding:"required"`
	OriginUrl string `form:"originUrl" binding:"required"`
	ThumbUrl string `form:"thumbUrl" binding:"required"`
	MarkUrl string `form:"markUrl"`
	AccessKeyId string `form:"accessKeyId" binding:"required"`
	AccessKeySecret string `form:"accessKeySecret" binding:"required"`
	CallBack string `form:"callBack"`
	BucketName string `form:"bucketName"`
	Endpoiont string `form:"endpoiont"`
}

type Thumb struct {
	OssClient *oss.Client
	OssBucket *oss.Bucket
	Jdata JsonData
	RemoveFiles []string
	LocalOrigin string
	OriginAngle int
	LocalThumb string
	ThumbModel model.ThumbModel
	CompressStatus bool
}

// 定时器 500毫秒一次心跳包
func Timer(timer func()) {
	log.Println("开启程序，监听goThumbQueue队列")
	ticker := time.NewTicker(500 * time.Millisecond)
	// 定义最大协程数
	task_maxnum := common.Config.MustValue("config", "task_maxnum")
	maxnum, _ := strconv.Atoi(task_maxnum)
	ch := make(chan bool, maxnum)
	for {
		select {
			case <-ticker.C:
				// 开启协程前塞入一个数据，如果20个槽塞满了，便会阻塞住
				ch <- true
				go func() {
					defer func() {
						<- ch
					}()
					timer()
				}()
		}
	}
}

func TimerFunc() {
	// 获取任务数据
	cr := common.ClientRedis.Get()
	defer func() {
		cr.Close()
	}()
	if cr.Err() != nil {
		log.Println("ClientRedis.Get错误", cr.Err().Error())
		return
	}
	data, err := cr.Do("rpop", "goThumbQueue")
	if err == nil && data != nil {
		jdata := JsonData{}
		err := json.Unmarshal(data.([]byte), &jdata)
		if err == nil {
			log.Println("开始处理队列数据:", jdata.Md5)
			//初始化缩略对象
			thumb := NewThumb()
			thumb.InitThumb(jdata)
			thumb.AutoCompress()
		} else {
			log.Println("队列数据解析错误", data)
		}
	} else {
		//log.Println("队列中没有数据要处理", common.WorkNum)
	}
}

func NewThumb() Thumb {
	return Thumb{}
}

// 自动判断使用哪种压缩
func (this *Thumb) AutoCompress() {
	defer this.DeferFunc()
	this.ThumbModel = model.GetThumb(this.Jdata.Md5)
	if this.ThumbModel.Md5 != "" && strings.Contains(this.ThumbModel.Thumb_url, this.Jdata.BucketName) {
		//图片已处理，跳过处理
		log.Println("图片已处理，跳过处理:", this.Jdata.Md5)
	} else {
		// 图片未处理,执行程序
		this.GetOriginFile()
		gocArr := []string{"png", "jpg", "jpeg", "gif", "bmp"}
		if common.In(this.ThumbModel.Ext, gocArr) {
			// go支持格式，程序缩略
			this.GoCompress()
		} else {
			// 调用dcraw缩略图片
			this.DcrawCompress()
		}
		//图片信息解析
		this.ImageAnalysis()
		//处理水印
		if this.Jdata.BucketName == "qsh-imgstore" {
			this.Watermark()
		}
	}
	//通知回调
	if this.Jdata.CallBack != "" {
		//执行回调函数
		common.CallBackFun(this.Jdata.CallBack, this.ThumbModel.Md5, this.ThumbModel.Thumb_url, this.ThumbModel.Mark_url)
	}
	// 处理完成
	this.Success()
}

// 图片打水印
func (this *Thumb) Watermark() {
	// 利用Oss设置水印并下载到本地
	text := ""
	if (this.Jdata.Id == "") {
		text = common.Base64url([]byte(strconv.Itoa(this.ThumbModel.ID)))
	} else {
		text = common.Base64url([]byte(this.Jdata.Id))
	}
	style := "image/auto-orient,1/resize,m_lfit,w_1000,h_1000/quality,q_90"
	style += "/watermark,image_dGh1bWIvd2F0ZXJtYXJrLnBuZz94LW9zcy1wcm9jZXNzPWltYWdlL3Jlc2l6ZSxQXzIw,g_center,x_10,y_10" //图片水印
	style += "/watermark,type_d3F5LXplbmhlaQ,size_40,text_"+text+",color_FFFFFF,shadow_50,t_100,g_se,x_10,y_10" //文字水印
	objectKey := this.Jdata.ThumbUrl
	targetImageName := this.Jdata.MarkUrl
	process := fmt.Sprintf("%s|sys/saveas,o_%v", style, base64.URLEncoding.EncodeToString([]byte(targetImageName)))
	_, err := this.OssBucket.ProcessObject(objectKey, process)
	if err != nil {
		panic("水印处理失败:" + err.Error())
	}
}

// 转码图信息
func (this *Thumb) GetImageInfo() (int64, int64, int64) {
	var size int64
	var width int64
	var height int64
	fi, err := os.Stat(this.LocalOrigin)
	if err == nil {
		size = fi.Size()
	}
	fp, err := os.Open(this.LocalThumb)
	defer fp.Close()
	if err == nil {
		imageC, _, err := image.DecodeConfig(fp)
		if err == nil {
			width = int64(imageC.Width)
			height = int64(imageC.Height)
		}
	}
	return size, width, height
}

// image库压缩
func (this *Thumb) GoCompress() {
	fp, err := os.Open(this.LocalOrigin)
	defer fp.Close()
	if err != nil {
		panic("打开源图发生错误:" + err.Error())
	}
	imageT, _, err := image.Decode(fp)
	if err != nil {
		panic("图片解码发生错误:" + err.Error())
	}
	newFile, err := os.Create(this.LocalThumb)
	defer newFile.Close()
	if err != nil {
		panic("创建文件发生错误:" + err.Error())
	}
	//bounds := imageT.Bounds()
	//log.Println("图片尺寸信息: ", bounds.Dx(), bounds.Dy())
	if this.OriginAngle > 0 {
		//图片旋转
		imageT = this.RotateImage(imageT, this.OriginAngle)
	}
	err = jpeg.Encode(newFile, imageT, &jpeg.Options{Quality: 75})
	if err != nil {
		panic("编码为jpg格式时发生错误:" + err.Error())
	}
	// 上传OSS
	this.OssUploadFile(this.Jdata.ThumbUrl, this.LocalThumb)
}

// convert命令 压缩
func (this *Thumb) ConvertCompress() {
	//图片缩略处理
	command := exec.Command("convert", this.LocalOrigin, `-quality`, `80`, `-resize`, `80%`, this.LocalThumb)
	var out bytes.Buffer
	var stderr bytes.Buffer
	command.Stdout = &out
	command.Stderr = &stderr
	// 执行命令 convert
	if err := command.Run(); err != nil {
		log.Println("convert 命令处理失败:", stderr.String())
		return
	}
	// 检测缩略图是否成功
	_, err := os.Stat(this.LocalThumb)
	if err != nil {
		panic("未检测到convert生成的缩略图")
	}
	// 上传OSS
	this.OssUploadFile(this.Jdata.ThumbUrl, this.LocalThumb)
}

// dcrawm命令 压缩
func (this *Thumb) DcrawCompress() {
	//图片缩略处理
	ext := path.Ext(this.LocalOrigin)
	thumbName := strings.Replace(this.LocalOrigin, ext, ".thumb.jpg", 1)
	command := exec.Command("dcraw", `-4`, `-h`, `-e`, this.LocalOrigin)
	var out bytes.Buffer
	var stderr bytes.Buffer
	command.Stdout = &out
	command.Stderr = &stderr
	// 执行命令 dcraw
	if err := command.Run(); err != nil {
		log.Println("dcraw处理失败，调用convert命令")
		//dcraw处理失败，使用 convert 处理
		command = exec.Command("convert", this.LocalOrigin, `-quality`, `75`, `-resize`, `75%`, thumbName)
		command.Stdout = &out
		command.Stderr = &stderr
		if err := command.Run(); err != nil {
			panic("convert处理失败：" + stderr.String())
		}
	}
	// 检测缩略图是否成功
	_, err := os.Stat(thumbName)
	if err != nil {
		panic("未检测到dcraw生成的缩略图")
	}
	os.Rename(thumbName, this.LocalThumb)
	// 上传OSS
	this.OssUploadFile(this.Jdata.ThumbUrl, this.LocalThumb)
}

// 本地文件上传OSS
func (this *Thumb) OssUploadFile(objectKey string, filePath string) bool {
	err := this.OssBucket.UploadFile(objectKey, filePath, 10*1024*1024, oss.Routines(3), oss.Checkpoint(true, ""))
	if err != nil {
		log.Println("OSS 上传失败 :", err.Error())
		return false
	}
	return true
}

// 图片数据分析
func (this *Thumb) ImageAnalysis() {
	//处理成功记录MD5和缩略文件名
	thumb := model.GetThumb(this.Jdata.Md5)
	if thumb.Md5 == "" {
		// 常规数据
		this.ThumbModel.Md5 = this.Jdata.Md5
		this.ThumbModel.Origin_url = common.OssShowUrl(this.Jdata.OriginUrl, this.Jdata.Endpoiont, this.Jdata.BucketName)
		this.ThumbModel.Thumb_url = common.OssShowUrl(this.Jdata.ThumbUrl, this.Jdata.Endpoiont, this.Jdata.BucketName)
		if this.Jdata.MarkUrl != "" {
			this.ThumbModel.Mark_url = common.OssShowUrl(this.Jdata.MarkUrl, this.Jdata.Endpoiont, this.Jdata.BucketName)
		}
		this.ThumbModel.Access_key_id = this.Jdata.AccessKeyId
		this.ThumbModel.Bucket_name = this.Jdata.BucketName
		this.ThumbModel.Created_at = time.Now().Unix()
		this.ThumbModel.Updated_at = time.Now().Unix()
		// 优先写入数据库
		model.CreateThumb(&this.ThumbModel)
		// 图片大小，尺寸，比例
		this.ThumbModel.Size, this.ThumbModel.Width, this.ThumbModel.Height = this.GetImageInfo()
		scale := float64(this.ThumbModel.Width) / float64(this.ThumbModel.Height)
		this.ThumbModel.Scale = float64(int64(scale * 1000)) / 1000
		// 色彩建模
		fp, err := os.Open(this.LocalThumb)
		defer fp.Close()
		if err == nil {
			img, err := jpeg.Decode(fp)
			if err == nil {
				// 提取图片6种颜色
				colors := quantize.Image(img, 2)
				palette := make([]color.Color, len(colors))
				for index, clr := range colors {
					palette[index] = clr
				}
				pcolors, err := json.Marshal(palette)
				if err == nil {
					this.ThumbModel.Colors = string(pcolors)
				}
			}
		}
		// 华为云 图片标签解析
		this.ThumbModel.Tags = GetHuaweiInstance().GetImageTagging(common.OssShowUrl(this.ThumbModel.Thumb_url, this.Jdata.Endpoiont, this.Jdata.BucketName))
		// 修改数据库
		model.UpdateThumb(&this.ThumbModel)
	}
}

// 旋转缩略图
func (this *Thumb) RotateImage(m image.Image, angle int) image.Image {
	if angle == 90 {
		rotate90 := image.NewRGBA(image.Rect(0, 0, m.Bounds().Dy(), m.Bounds().Dx()))
		// 矩阵旋转
		for x := m.Bounds().Min.Y; x < m.Bounds().Max.Y; x++ {
			for y := m.Bounds().Max.X - 1; y >= m.Bounds().Min.X; y-- {
				//  设置像素点
				rotate90.Set(m.Bounds().Max.Y-x, y, m.At(y, x))
			}
		}
		return rotate90
	} else if angle == 180 {
		rotate180 := image.NewRGBA(image.Rect(0, 0, m.Bounds().Dx(), m.Bounds().Dy()))
		// 矩阵旋转
		for x := m.Bounds().Min.X; x < m.Bounds().Max.X; x++ {
			for y := m.Bounds().Min.Y; y < m.Bounds().Max.Y; y++ {
				//  设置像素点
				rotate180.Set(m.Bounds().Max.X-x, m.Bounds().Max.Y-y, m.At(x, y))
			}
		}
		return rotate180
	} else if angle == 270 {
		rotate270 := image.NewRGBA(image.Rect(0, 0, m.Bounds().Dy(), m.Bounds().Dx()))
		// 矩阵旋转
		for x := m.Bounds().Min.Y; x < m.Bounds().Max.Y; x++ {
			for y := m.Bounds().Max.X - 1; y >= m.Bounds().Min.X; y-- {
				// 设置像素点
				rotate270.Set(x, m.Bounds().Max.X-y, m.At(y, x))
			}
		}
		return rotate270
	}
	return m
}

// 处理完成
func (this *Thumb) Success() {
	log.Println("图片处理完成 :", this.ThumbModel.Md5)
	this.CompressStatus = true
}

// 下载源图到本地
func (this *Thumb) GetOriginFile() {
	baseName := path.Base(this.Jdata.OriginUrl)
	ext := path.Ext(this.Jdata.OriginUrl)
	now := strconv.Itoa(int(time.Now().Unix()))
	this.LocalOrigin = common.SoftDir + "/temp/" + strings.Replace(baseName, ext, "_"+ now + ext, 1)
	err := this.OssBucket.GetObjectToFile(this.Jdata.OriginUrl, this.LocalOrigin)
	if err != nil {
		panic("OSS 下载图片错误:" + err.Error())
	}
	os.Chmod(this.LocalOrigin, 0777)
	ext = path.Ext(this.Jdata.ThumbUrl)
	this.LocalThumb = path.Dir(this.LocalOrigin) + "/" + strings.Replace(path.Base(this.Jdata.ThumbUrl), ext, "_"+ now + ext, 1)
	this.ThumbModel.Ext = strings.ToLower(path.Ext(this.LocalOrigin)[1:])

	//读取exif信息
	file, err := os.Open(this.LocalOrigin)
	if err != nil {
		log.Println("打开源图错误: ", err)
		return
	}
	defer file.Close()

	//exif.RegisterParsers(mknote.All...)
	x, err := exif.Decode(file)
	if err != nil {
		log.Println("解析源图exif失败: ", err)
		return
	}
	orient, err := x.Get(exif.Orientation)
	if err == nil {
		orientVal, err := orient.Int(0)
		if err == nil {
			if orientVal == 6 {
				this.OriginAngle = 90
			} else if orientVal == 3 {
				this.OriginAngle = 180
			} else if orientVal == 8 {
				this.OriginAngle = 270
			}
		}
	}

	// exif信息
	strExif, err := x.MarshalJSON()
	if err == nil {
		this.ThumbModel.Exif = string(strExif)
	}
	// 拍摄时间
	tm, err := x.DateTime()
	if err == nil {
		this.ThumbModel.Shot_at = tm.Unix()
	}
	// 经纬度
	lat, long, err := x.LatLong()
	if err == nil && !math.IsNaN(long) && !math.IsNaN(lat) {
		this.ThumbModel.Long = strconv.FormatFloat(long, 'f', 7, 64)
		this.ThumbModel.Lat = strconv.FormatFloat(lat, 'f', 7, 64)
		//逆地理编码
		this.ThumbModel.Address = common.AmapGeocodeRegeo(this.ThumbModel.Long, this.ThumbModel.Lat)
	}
}

// 捕获异常/删除临时文件
func (this *Thumb) DeferFunc() {
	if r := recover(); r != nil {
		log.Println("异常处理：", this.Jdata.Md5, r)
	}
	for _, file := range []string{this.LocalOrigin, this.LocalThumb} {
		os.Remove(file)
	}
}

//初始化类
func (this *Thumb) InitThumb(data JsonData) {
	this.Jdata = data
	u, err := url2.Parse(data.OriginUrl)
	if err != nil {
		log.Error("OriginUrl解析失败", err.Error())
		os.Exit(0)
	}

	this.Jdata.OriginUrl = u.Path[1:]
	host := strings.Split(u.Host, ".")
	this.Jdata.BucketName = host[0]
	this.Jdata.Endpoiont = "https://" + strings.Replace(u.Host, host[0] + ".", "", 1)
	endpoiont := this.Jdata.Endpoiont
	accessKeyId := data.AccessKeyId
	accessKeySecret := data.AccessKeySecret
	bucketName := this.Jdata.BucketName

	// 创建OSSClient实例。
	//endpoiont := common.Config.MustValue("oss", "endpoiont")
	//accessKeyId := common.Config.MustValue("oss", "access_key_id")
	//accessKeySecret := common.Config.MustValue("oss", "access_key_secret")
	//bucketName := common.Config.MustValue("oss", "bucket_name")
	if common.IsEnvByProduction() == true {
		//正式环境
		endpoiont = strings.Replace(endpoiont, ".aliyuncs.com", "-internal.aliyuncs.com", 1)
	}
	this.OssClient, err = oss.New(endpoiont, accessKeyId, accessKeySecret)
	if err != nil {
		log.Println("OSS初始化失败", err.Error())
		os.Exit(0)
	}
	// 获取存储空间
	this.OssBucket, err = this.OssClient.Bucket(bucketName)
	if err != nil {
		log.Println("OSS初始化失败", err.Error())
		os.Exit(0)
	}
}
