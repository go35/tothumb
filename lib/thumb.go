package lib

import (
	"bytes"
	"fmt"
	"github.com/aliyun/aliyun-oss-go-sdk/oss"
	"image"
	"image/jpeg"
	_ "golang.org/x/image/bmp"
	_ "image/gif"
	_ "image/png"
	"main/log"
	"os"
	"os/exec"
	"path"
	"strconv"
	"strings"
	"time"
)

type Thumb struct {
	OssClient *oss.Client
	OssBucket *oss.Bucket
	Jdata *JsonData
	RemoveFiles []string
	FileExt string
}

func NewThumb() *Thumb {
	return &Thumb{}
}

// 自动判断使用哪种压缩
func (this *Thumb) AutoCompress() {
	defer this.RemoveAll()
	thumbName, err := ClientRedis.HGet("goThumbAcks", this.Jdata.Md5).Result()
	if err == nil && thumbName != "" {
		//图片已处理，跳过处理
		log.Println("图片已处理，跳过处理:", this.Jdata.Md5, thumbName)
		this.Jdata.ThumbName = thumbName
	} else {
		//图片未处理,执行程序
		this.FileExt = strings.ToUpper(path.Ext(this.Jdata.OssUrl))
		gocArr := []string{".PNG", ".JPG", ".JPEG", ".GIF", ".BMP"}
		if In(this.FileExt, gocArr) {
			//go支持格式，程序缩略 this.GoCompress()
			//暂时改为convert处理图片试试效率
			this.ConvertCompress()
		} else {
			//调用dcraw缩略图片
			this.DcrawCompress()
		}
	}
	//处理完成
	this.Success()
}

//image库 压缩
func (this *Thumb) GoCompress() {
	tempName, err := this.GetObjectToFile()
	if err != nil {
		log.Println("OSS 下载图片错误:", err.Error())
		return
	}
	this.RemoveFiles = append(this.RemoveFiles, tempName)
	//图片缩略处理
	thumbName := path.Dir(tempName) + "/" + this.Jdata.ThumbName
	fp, err := os.Open(tempName)
	defer fp.Close()
	if err != nil {
		log.Println("打开文件发生错误 :", err.Error())
		return
	}
	imageT, _, err := image.Decode(fp)
	if err != nil {
		log.Println("图片解码发生错误 :", err.Error())
		return
	}
	//bounds := imageT.Bounds()
	//log.Println("图片尺寸信息: ", bounds.Dx(), bounds.Dy())

	newFile, err := os.Create(thumbName)
	if err != nil {
		log.Println("创建文件发生错误 :", err.Error())
		return
	}
	defer newFile.Close()

	err = jpeg.Encode(newFile, imageT, &jpeg.Options{Quality: 80})
	if err != nil {
		log.Println("编码为jpg格式时发生错误 :", err.Error())
		return
	}
	//上传OSS
	objectKey := path.Dir(this.Jdata.OssUrl) + "/" + this.Jdata.ThumbName
	this.OssUploadFile(objectKey, thumbName)
	this.RemoveFiles = append(this.RemoveFiles, thumbName)
}


//convert命令 压缩
func (this *Thumb) ConvertCompress() {
	tempName, err := this.GetObjectToFile()
	if err != nil {
		log.Println("OSS 下载图片错误:", err.Error())
		return
	}
	this.RemoveFiles = append(this.RemoveFiles, tempName)
	_, err = os.Stat(tempName)
	if err != nil {
		fmt.Println("tempName 错误 ", err)
	}
	fmt.Println("临时文件下载成功", tempName)
	//图片缩略处理
	thumbName := path.Dir(tempName) + "/" + this.Jdata.ThumbName
	command := exec.Command("convert", tempName, `-quality`, `80`, `-resize`, `80%`, thumbName)
	var out bytes.Buffer
	var stderr bytes.Buffer
	command.Stdout = &out
	command.Stderr = &stderr
	//执行命令 convert
	if err := command.Run(); err != nil {
		log.Println("convert 命令处理失败:", stderr.String())
		return
	}
	//检测缩略图时间3秒
	expire := time.Now().Unix() + 3
	for {
		if time.Now().Unix() < expire {
			_, err := os.Stat(thumbName)
			if err == nil {
				break
			}
		} else {
			log.Println("未检测到convert生成的缩略图:")
			return
		}
	}
	//上传OSS
	objectKey := path.Dir(this.Jdata.OssUrl) + "/" + this.Jdata.ThumbName
	this.OssUploadFile(objectKey, thumbName)
	this.RemoveFiles = append(this.RemoveFiles, thumbName)
}

//dcrawm命令 压缩
func (this *Thumb) DcrawCompress() {
	tempName, err := this.GetObjectToFile()
	if err != nil {
		log.Println("OSS 下载图片错误:", err.Error())
		return
	}
	this.RemoveFiles = append(this.RemoveFiles, tempName)
	_, err = os.Stat(tempName)
	if err != nil {
		fmt.Println("tempName 错误 ", err)
	}
	fmt.Println("临时文件下载成功", tempName)
	ext := path.Ext(tempName)
	thumbName := strings.Replace(tempName, ext, ".thumb.jpg", 1)
	command := exec.Command("dcraw", `-4`, `-h`, `-e`, tempName)
	var out bytes.Buffer
	var stderr bytes.Buffer
	command.Stdout = &out
	command.Stderr = &stderr
	//执行命令 dcraw
	if err := command.Run(); err != nil {
		log.Println("dcraw处理失败，调用convert命令:", stderr.String())
		//dcraw处理失败，使用 convert 处理
		command = exec.Command("convert", tempName, `-quality`, `75`, `-resize`, `75%`, thumbName)
		command.Stdout = &out
		command.Stderr = &stderr
		if err := command.Run(); err != nil {
			log.Println("convert处理失败:", stderr.String())
			return
		}
	}
	//检测缩略图时间3秒
	expire := time.Now().Unix() + 3
	for {
		if time.Now().Unix() < expire {
			_, err := os.Stat(thumbName)
			if err == nil {
				break
			}
		} else {
			log.Println("未检测到dcraw生成的缩略图:")
			return
		}
	}
	//上传OSS
	objectKey := path.Dir(this.Jdata.OssUrl) + "/" + this.Jdata.ThumbName
	this.OssUploadFile(objectKey, thumbName)
	this.RemoveFiles = append(this.RemoveFiles, thumbName)
}

//本地文件上传OSS
func (this *Thumb) OssUploadFile(objectKey string, filePath string) bool {
	err := this.OssBucket.UploadFile(objectKey, filePath, 10*1024*1024, oss.Routines(3), oss.Checkpoint(true, ""))
	if err != nil {
		log.Println("OSS 上传失败 :", err.Error())
		return false
	}
	return true
}

//处理成功
func (this *Thumb) Success() {
	//处理成功记录MD5和缩略文件名
	ClientRedis.HSet("goThumbAcks", this.Jdata.Md5, this.Jdata.ThumbName)
	log.Println("图片缩略成功 :", this.Jdata.Md5)
	if this.Jdata.CallBack != "" {
		//执行回调函数
		thumbName := path.Dir(this.Jdata.OssUrl) + "/" + this.Jdata.ThumbName
		CallBackFun(this.Jdata.CallBack, this.Jdata.Md5, thumbName)
	}
}


//下载远程图片到本地
func (this *Thumb) GetObjectToFile() (string, error) {
	baseName := path.Base(this.Jdata.OssUrl)
	ext := path.Ext(this.Jdata.OssUrl)
	now := strconv.Itoa(int(time.Now().Unix()))
	tempName := SoftDir + "/temp/" + strings.Replace(baseName, ext, "_"+ now + ext, 1)
	err := this.OssBucket.GetObjectToFile(this.Jdata.OssUrl, tempName)
	if err == nil {
		os.Chmod(tempName, 0777)
	}
	return tempName, err
}

//删除临时文件
func (this *Thumb) RemoveAll() {
	for _, file := range this.RemoveFiles {
		os.Remove(file)
	}
}

//初始化类
func (this *Thumb) InitThumb(data *JsonData) {
	this.Jdata = data
	// 创建OSSClient实例。
	endpoiont := Config.MustValue("oss", "endpoiont")
	accessKeyId := Config.MustValue("oss", "access_key_id")
	accessKeySecret := Config.MustValue("oss", "access_key_secret")
	bucketName := Config.MustValue("oss", "bucket_name")
	if IsEnvByProduction() == true {
		//正式环境
		endpoiont = strings.Replace(endpoiont, ".aliyuncs.com", "-internal.aliyuncs.com", 1)
	}
	var err error
	this.OssClient, err = oss.New(endpoiont, accessKeyId, accessKeySecret)
	if err != nil {
		log.Println("OSS初始化失败", err.Error())
		os.Exit(0)
	}
	// 获取存储空间。
	this.OssBucket, err = this.OssClient.Bucket(bucketName)
	if err != nil {
		log.Println("OSS初始化失败", err.Error())
		os.Exit(0)
	}
}