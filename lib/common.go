package lib

import (
	"encoding/json"
	"github.com/go-redis/redis"
	"github.com/unknwon/goconfig"
	"main/log"
	"net/http"
	url2 "net/url"
	"os"
	"sort"
	"time"
)

var (
	SoftDir string
	ClientRedis *redis.Client
	Config *goconfig.ConfigFile
)

type JsonData struct {
	Md5 string
	OssUrl string
	ThumbName string
	CallBack string
}

func IsEnvByProduction() bool {
	env := Config.MustValue("config", "env")
	if env == "production" {
		return true
	} else {
		return false
	}
}

func IsDir(path string) bool {
	s, err := os.Stat(path)
	if err != nil {
		return false
	}
	return s.IsDir()
}

func In(str string, str_array []string) bool {
	sort.Strings(str_array)
	index := sort.SearchStrings(str_array, str)
	if index < len(str_array) && str_array[index] == str {
		return true
	}
	return false
}

func CallBackFun(url string, md5 string, thumbName string) {
	p := url2.Values{}
	p.Add("md5", md5)
	p.Add("thumbName", thumbName)
	url += "?" + p.Encode()
	_, err := http.Get(url)
	if err != nil {
		log.Println("http get 回调 错误", err.Error())
	}
}

//定时器 500毫秒一次心跳包
func Timer(timer func()) {
	ticker := time.NewTicker(500*time.Millisecond)
	for {
		select {
		case <-ticker.C:
			go timer()
		}
	}
}

func TimerFunc() {
	//获取任务数据
	data, err := ClientRedis.RPop("goThumbQueue").Result()
	if err == nil && data != "" {
		//json := &JsonData {
		//	"140a9c5a4e125208e7425139c11c88a5",
		//	"uploads/140/a9c/140a9c5a4e125208e7425139c11c88a5.NEF",
		//	"140a9c5a4e125208e7425139c11c88a5_thumb.jpg",
		//	"",
		//}
		jdata := &JsonData{}
		err := json.Unmarshal([]byte(data), &jdata)
		if err == nil {
			log.Println("开始处理队列数据:", jdata.Md5)
			//初始化缩略对象
			thumb := NewThumb()
			thumb.InitThumb(jdata)
			thumb.AutoCompress()
		} else {
			log.Println("队列数据解析错误", data)
		}
	} else {
		//log.Println("队列中没有数据要处理")
	}
}
