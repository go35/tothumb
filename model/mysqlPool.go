package model

import (
	"fmt"
	"github.com/jinzhu/gorm"
	_ "github.com/go-sql-driver/mysql"
	"sync"
	"zrpc/common"
)

type MysqlPool struct {
}

var (
	instance *MysqlPool
	once sync.Once
	db *gorm.DB
	err error
)

func GetInstance() *MysqlPool {
	once.Do(func() {
		instance = &MysqlPool{}
		instance.InitPool()
	})
	return instance
}

func (this *MysqlPool) GetDb() (*gorm.DB) {
	return db
}

func (this *MysqlPool) InitPool() {
	common.Config.MustValue("mysql", "username")
	dsn := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?charset=utf8",
		common.Config.MustValue("mysql", "username"),
		common.Config.MustValue("mysql", "password"),
		common.Config.MustValue("mysql", "host"),
		common.Config.MustValue("mysql", "port"),
		common.Config.MustValue("mysql", "database"),
	)
	db, err = gorm.Open("mysql", dsn)
	if err != nil {
		panic("mysql连接失败" + err.Error())
	}
	//设置表前缀
	gorm.DefaultTableNameHandler = func(db *gorm.DB, defaultTableName string) string {
		prefix := common.Config.MustValue("mysql", "prefix")
		fmt.Println("前缀---", prefix)
		if prefix != "" {
			defaultTableName = prefix + defaultTableName
		}
		return defaultTableName
	}
	// 空闲状态下最大连接数
	db.DB().SetMaxIdleConns(50)
	// 打开最大连接数
	db.DB().SetMaxOpenConns(50)
}
