package model

type ThumbModel struct {
	ID int `gorm:"primary_key"`
	Md5 string `gorm:"type:char(32);unique"`
	Width int64
	Height int64
	Scale float64
	Size int64
	Ext string
	Long string
	Lat string
	Origin_url string
	Thumb_url string
	Mark_url string
	Access_key_id string
	Bucket_name string
	Tags string
	Address string
	Exif string
	Colors string
	Shot_at int64
	Created_at int64
	Updated_at int64
}

// 设置表名
func (this *ThumbModel) TableName() string {
	return "img_thumb"
}

func GetThumb(md5 string) ThumbModel {
	db := GetInstance().GetDb()
	thumb := ThumbModel{}
	db.Where("md5 = ?", md5).First(&thumb)
	return thumb
}

func CreateThumb(data *ThumbModel) {
	db := GetInstance().GetDb()
	db.Create(data)
}

func UpdateThumb(data *ThumbModel) {
	db := GetInstance().GetDb()
	db.Save(data)
}

