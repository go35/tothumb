# 图库解码，打水印，图像识别等

#### 项目介绍
- 图库解码，打水印，图像识别等

#### 安装环境
- golang 1.15

#### 目录文件
- 根目录/log  日志目录
- 根目录/temp  临时文件目录
- 根目录/main.go 程序源文件

#### 编译流程
- CD 根目录
- 执行 go build
- 根目录生成可执行文件，无需依赖外部扩展

#### 配置信息修改
- main.go 文件第47行
- OssCfg OSS配置
- RedisCfg Redis配置

#### 实现原理
- 服务方：go程序监听redis中list结构数据，类似消息队列入队出队的方式处理JSON数据
- 应用方：只需要往指定的list中添加JSON数据即可

#### JSON数据格式
~~~
{
    'Id' => '图片ID，文字水印，不填默认自增ID',
    'Md5' => '751c213d05e60b0b17ebeff32b356311',
    'originUrl' => 'uploads/origin/751/c21/751c213d05e60b0b17ebeff32b356311.jpg',
    'thumbUrl' => 'uploads/thumb/751/c21/751c213d05e60b0b17ebeff32b356311.jpg',
    'markUrl' => 'uploads/mark/751/c21/751c213d05e60b0b17ebeff32b356311.jpg',
    'callBack' => '回调地址',
}
~~~



